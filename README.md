# commercebot-detectcarcolor-files



## Downloading the Files

The Files can be downloaded either manually from here or by cloning the repository in a seperate folder and then moving the files to the required location.

For cloning, enter the following in command prompt/ Powershell/ Git Bash in the new seperate folder:

    git clone https://gitlab.com/saurabh-ixxo/commercebot-detectcarcolor-files.git

This clones the folders and files in your folder